<?php
session_start();

if(!isset($_SESSION["username"])){
	// don't allow people who are logged out to delete things
	header("Location: main.php");
	exit;
}
require "database.php";
// check if this is a story deletion, or user, or comment deletion
if(isset($_GET['id'])){
	// delete story and associated comments
	// grab passed story id from URL
	$story_id = htmlentities(urldecode($_GET['id']));
	
	// submit query to database to delete any comments associated with this story
	$stmt = $mysqli->prepare("delete from comments where story_id= ?");
	if(!$stmt){
		printf("Query Prep Failed for Comments Deletion: %s\n", $mysqli->error);
		exit;
	}
	$stmt->bind_param('i', $story_id);
	$stmt->execute();
	$stmt->close();
	
	// submit query to database to delete the story from database
	$stmt = $mysqli->prepare("delete from stories where pk_story_id= ?");
	if(!$stmt){
		printf("Query Prep Failed for Story Deletion: %s\n", $mysqli->error);
		exit;
	}
	$stmt->bind_param('i', $story_id);
	$stmt->execute();
	$stmt->close();
	header("Location: main.php");
}
elseif(isset($_GET["cid"])){
	// delete comment only
	// grab passed comment id from URL
	$comment_id = urldecode($_GET['cid']);
	// grab story id from url as well
	$story_id = urldecode($_GET['sid']);
	// submit query to database to delete any comments associated with this id
	$stmt = $mysqli->prepare("delete from comments where pk_comment_id= ?");
	if(!$stmt){
		printf("Query Prep Failed for Comment Deletion: %s\n", $mysqli->error);
		exit;
	}
	$stmt->bind_param('i', $comment_id);
	$stmt->execute();
	$stmt->close();
	// return to page for this article
	header("Location: article.php?id=".urlencode($story_id)."#footer");
	exit();
}
elseif(isset($_GET["uid"])){
	// delete a user account, their associated stories, and comments
	// grab passed user id from URL
	$username = htmlentities(urldecode($_GET['uid']));
	if($username == $_SESSION["username"]){
		// double check that the user who is logged in is deleting their own account
		
		// submit query to database to delete any comments associated with this user
		$stmt = $mysqli->prepare("delete from comments where commenter= ?");
		if(!$stmt){
			printf("Query Prep Failed for Comments Deletion: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $username);
		$stmt->execute();
		if($stmt->errno){
			printf("comments failed: %s\n", $stmt->error);
		}
		$stmt->close();
		
		// submit query to database to delete any comments associated with this user's stories
		$stmt = $mysqli->prepare("delete from comments where story_author= ?");
		if(!$stmt){
			printf("Query Prep Failed for Comments Deletion from other's stories: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $username);
		$stmt->execute();
		if($stmt->errno){
			printf("comments deletion from others' stories failed: %s\n", $stmt->error);
		}
		$stmt->close();
		
		// submit query to database to delete the user's stories from database
		$stmt = $mysqli->prepare("delete from stories where author=?");
		if(!$stmt){
			printf("Query Prep Failed for Story Deletion: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $username);
		$stmt->execute();
				if($stmt->errno){
			printf("stories failed: %s\n", $stmt->error);
		}
		$stmt->close();
		
		// submit query to database to delete this user's records
		$stmt = $mysqli->prepare("delete from users where username=?");
		if(!$stmt){
			printf("Query Prep Failed for User Deletion: %s\n", $mysqli->error);
			exit;
		}
		$stmt->bind_param('s', $username);
		$stmt->execute();
		if($stmt->errno){
			printf("users failed: %s\n", $stmt->error);
		}
		$stmt->close();
	}
	// log user out by redirecting to logout page
	header("Location: logout.php");
}
?>
