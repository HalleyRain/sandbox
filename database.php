<?php
// will be required in all php scripts which need database access
$mysqli = new mysqli('localhost', 'phpuser', 'phppass', 'newsflash');

if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}
?>