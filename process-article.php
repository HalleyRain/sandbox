<?php
    session_start();
    // is this a new article or an edit to a previous one?
    $isEdit = isset($_GET["id"]);
    // access database
    require 'database.php';
    if (!$isEdit){
        // Grab inputs from form and session
        $title = htmlentities($_POST['story-title']);
        $body = htmlentities($_POST['story-text']);
        // CSRF protection
        if(!hash_equals($_SESSION['token'], $_POST['token'])){
	         die("Request forgery detected");
        }
        $author = $_SESSION["username"];
        //submit required story info to database
        $stmt = $mysqli->prepare("insert into stories (title, author, story_text) values (?,?,?)");
        if(!$stmt){
            $_SESSION["article-status"] = "Error while preparing to upload story to database.";
             header("Location: submit-article.php");
             exit();
        }
        $stmt->bind_param('sss', $title, $author, $body);
        $stmt->execute();
        if($stmt->errno) {
             $_SESSION["article-status"] = "Error while uploading story to database.".$stmt->error;
             header("Location: submit-article.php");
             exit();
        }
        $stmt->close();
        // store the id of the story just inserted
        $story_id = $mysqli->insert_id;

    }else{
        // update database info instead of inserting into table
        // Grab inputs from form and session
        $title = htmlentities($_POST['story-title']);
        $body = htmlentities($_POST['story-text']);
        // CSRF protection
        if(!hash_equals($_SESSION['token'], $_POST['token'])){
	         die("Request forgery detected");
        }
        $story_id = htmlentities(urldecode($_GET["id"]));
        $stmt = $mysqli->prepare("update stories set title=?, story_text=? where pk_story_id=?");
        if($stmt->errno){
            $_SESSION["article-status"] = "Error while preparing to update story in database.";
             header("Location: submit-article.php?id=".$story_id);
             exit();
        }
        $stmt->bind_param('ssd', $title, $body,  $story_id);
        $stmt->execute();
        if($stmt->errno) {
             $_SESSION["article-status"] = "Error while updating story in database.".$stmt->error;
             header("Location: submit-article.php?id=".$story_id);
             exit();
        }
        $stmt->close();
    }

    // Inserting file into database code adapted from: http://phppot.com/php/mysql-blob-using-php/
    // if an image was submitted to the form (only an option for new story submissions, not edits)
    if(!$isEdit){
        if(is_uploaded_file($_FILES["story-image"]["tmp_name"])) {
            // check that filename of photo is valid
            // get the filename from the input form
            echo $_FILES['story-image']['tmp_name'];
            $filename = basename($_FILES['story-image']['name']);
            if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
               $_SESSION["article-status"] = htmlentities($filename)." is an invalid filename. Unable to upload a filename with spaces.";
               header("Location: submit-article.php");
               exit();
            }
            // code for checking image type and size adapted from https://stackoverflow.com/questions/9153224/how-to-limit-file-upload-type-file-size-in-php
            // check that the type of file is an image
            $acceptable = array(
                'image/jpeg',
                'image/jpg',
                'image/png'
            );
            // check that the size of an image is not 0 or too large (over  1MB)
            if(($_FILES['story-image']['size'] >= 2000000) || ($_FILES["story-image"]["size"] == 0)) {
                $_SESSION["article-status"] = 'Picture file is too large. File must be less than 2 megabytes.';
                header("Location: submit-article.php");
                exit();
            }
            // check that file is of an acceptable type
            if((!in_array($_FILES['story-image']['type'], $acceptable)) && (!empty($_FILES["story-image"]["type"]))) {
                $_SESSION["article-status"] = 'Invalid file type. Only JPG and PNG types are accepted.';
                header("Location: submit-article.php");
                exit();
            }
            //// get image data and type from $_FILES
            $contents = file_get_contents($_FILES['story-image']['tmp_name']);
            if(!$contents){
                 $_SESSION["article-status"] = 'Picture file is too large or took too long to upload. Try a smaller file.';
                header("Location: submit-article.php");
                exit();
            }
            $imgData =addslashes($contents);
            $imageProperties = getimageSize($_FILES['story-image']['tmp_name']);
            $imageType = $imageProperties['mime'];

            $stmt = $mysqli->prepare("UPDATE stories SET story_image = '{$imgData}', image_type = '{$imageType}' where pk_story_id= '{$story_id}'");
            if(!$stmt){
                $_SESSION["article-status"] = "Error while loading image to database. Try a smaller image size. Your story uploaded successfully without the image.";
                header("Location: submit-article.php");
                exit();  
            }
            $stmt->bind_param();
            $stmt->execute();
            if($stmt->errno) {
                 $_SESSION["article-status"] = "Error while adding image in database.".$stmt->error;
                 header("Location: submit-article.php");
                 exit();
            }
            $stmt->close();
        }
    }
    if($_POST["link"]!=null){
        $ext_link = htmlentities($_POST["link"]);
        // code for filtering links adapted from https://www.w3schools.com/php/filter_validate_url.asp
        if (!filter_var($ext_link, FILTER_VALIDATE_URL)) {
            $_SESSION["article-status"] = "Please enter a valid URL as the external link.";
            if($isEdit){
                header("Location: submit-article.php?id=".$story_id);
            }else{
                header("Location: submit-article.php");
            }
            exit();
        }
        // if a valid link was submitted, send to database
        $stmt = $mysqli->prepare("update stories set ext_link=? where pk_story_id=?");
        if(!$stmt){
            $_SESSION["article-status"] = "Error while preparing to upload link to database.";
             header("Location: submit-article.php");
             exit();
        }
        $stmt->bind_param('sd', $ext_link, $story_id);
        $stmt->execute();
        if($stmt->errno) {
             $_SESSION["article-status"] = "Error while uploading link to database.".$stmt->error;
             header("Location: submit-article.php");
             exit();
        }
        $stmt->close();
    }

    // processing successful; redirect back to main page
    header("Location: main.php");
    exit();
?>
