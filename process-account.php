<?php
    session_start();
    //if account creation button has been pressed, enter info into database
    require 'database.php';
    $isEdit = isset($_SESSION["username"]);

    if(!$isEdit){
        $new_username = $_POST['username'];
        $unhashed_password = $_POST['password'];

        $hashed_password = password_hash($unhashed_password, PASSWORD_DEFAULT);

        $stmt = $mysqli->prepare("insert into users (username, password) values (?, ?)");
        if(!$stmt){
            $_SESSION["account-status"] = "Query Prep Failed: ".$mysqli->error;
            header("Location: new-account.php");
            exit;
        }
        $stmt->bind_param('ss', $new_username, $hashed_password);
        $stmt->execute();
        if($stmt->errno){
            $_SESSION["account-status"] = "This username is already in use. Please try another.";
            header("Location: new-account.php");
            exit();
        }
        $stmt->close();
        // if user was registered, log in to session
        $_SESSION["username"] = $new_username;
        $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32)); // generate a 32-byte random string
        //send back to main as new user
        header("Location: main.php");
    }
    else{
        echo "edit mode";
        // a user is logged in and has entered a new password to be changed
        $new_password = htmlentities($_POST['password']);
        // CSRF protection
        if(!hash_equals($_SESSION['token'], $_POST['token'])){
	          die("Request forgery detected");
        }
        $hashed_password = password_hash($new_password, PASSWORD_DEFAULT);

        $stmt = $mysqli->prepare("update users set password=? where username=?");
        if(!$stmt){
            $_SESSION["account-status"] = "Query Prep update password Failed: ".$mysqli->error;
            header("Location: new-account.php");
            exit;
        }
        $stmt->bind_param('ss', $hashed_password, $_SESSION["username"]);
        $stmt->execute();
        if($stmt->errno){
            $_SESSION["account-status"] = "There was an error changing the password. Please try another.";
            header("Location: new-account.php");
            exit();
        }
        $stmt->close();
        //logout to force log in with new username
        header("Location: logout.php");
        exit;
    }
?>
