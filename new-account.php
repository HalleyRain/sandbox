<!DOCTYPE html>
<head>
<title>News⚡Flash</title>
    <link href="custom-stylesheet.css" rel="stylesheet" type="text/css" >
</head>
<body>
	<?php
	// only allow this page to be visited if no one is logged in
	session_start();
	if(isset($_SESSION["username"])){
		header("Location: main.php");
	}
	include "navbar.php"; ?>
		<h3>Enter an username and password to register a new account and log-in.</h3><hr>
		<?php
		// Display errors which may have occured in account creation.
    // Alert code adapted from https://www.w3schools.com/bootstrap/bootstrap_alerts.asp -->
    if(isset( $_SESSION["account-status"])){
        echo $_SESSION["account-status"];
        // once alert is displayed, wipe from session variable
        unset($_SESSION["account-status"]);
    }
		?>
		<form method="POST" action="process-account.php">
				<label for="username">Username</label><!--create textbox that accepts username -->
				<input type="text" maxlength="20" required name="username" id="username">
				<label for="password">Password</label><!--create textbox that accepts password -->
				<input type="password" maxlength="255" required name="password" id="password" >
				<!--submit form to create account & redirect to main page -->
				<input type="submit" class="btn btn-primary" value="Create Account" name="create-account"/>
        <!-- security token input -->
        <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
		</form>
</body>
</html>
