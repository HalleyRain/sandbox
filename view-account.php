<!doctype html>
<html lang="en">
  <head>
    <title>News⚡Flash</title>
    <link href="custom-stylesheet.css" rel="stylesheet" type="text/css" >
  </head>
  <body>
    <?php
        session_start();
        //retrieve username to display
        $displayUser = htmlentities($_GET["user"]);

        include "navbar.php";
        echo '<div class="newsfeed">';
                // check if logged in user is viewing their account
        if(isset($_SESSION["username"])){
            if(($displayUser == $_SESSION["username"])){
                $isSelf = true;
            }else{
              $isSelf = false;
            }
        }else{
            $isSelf = false;
        }
        
        // display author's name as title
        echo "<h1>Stories by ".$displayUser."
            </h1>";
        // allow options to delete account and change username if viewing own account page
        if($isSelf){
            echo "<a href='change-account.php'>Change Password</a>";
            echo " <a href='delete.php?uid=".urlencode($displayUser)."' >Delete User Account</a>";
            echo"<hr>";
        }
        require 'database.php';
        //select stories by this user to display
        $stmt = $mysqli->prepare("select * from stories where author=?");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $stmt->bind_param('s', $displayUser);
        $stmt->execute();
        $result = $stmt->get_result();
        // loop through all stories in this user's history 
        while($row = $result->fetch_assoc()){
           if($row['story_image'] != null){
            // if an image was submitted with the story, display here
              echo "<img src='data:".$row['image_type'].";base64,".base64_encode( $row['story_image'])."' alt='story image' class = 'story-img'>";
           }else{
            // display default "news" image
              echo "<img src='default_img.jpg' alt='story image' class = 'story-img'>";
           }
           echo "<h3>".$row['title']."</h3>";
          // isolate the lead of this story text to display here
          if(strlen($row['story_text']) > 150){
            $displayCutPos=strpos($row["story_text"], ' ', 150);
            $storyLead = substr($row["story_text"],0,$displayCutPos)."...";
          } else{
            $storyLead = $row["story_text"];
          }
          $story_id = $row["pk_story_id"];
          echo "<p>".$storyLead."</p>
                <!-- provide read more link to article-view script and use URL method to pass unique story id to the article php script. -->
                <br><a href='article.php?id=".urlencode($story_id)."'>Read More</a>";
          if(isset($_SESSION["username"])){
            if($_SESSION["username"] == $row["author"]){
              //if current user owns this story, display a delete and edit button
              echo " <a href='submit-article.php?id=".urlencode($story_id)."'>Edit</a>";
              echo " <a href='delete.php?id=".urlencode($story_id)."'>Delete</a><hr>";
            }
          }
        } // /while loop
        $stmt->close();
      ?>
      </div>
</body>
</html>