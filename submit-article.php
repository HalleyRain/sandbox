<?php
    // verify that a user is logged in before visiting this page; otherwise redirect to main page
    session_start();
    if(!isset($_SESSION["username"])){
        // no one is logged in; redirect to main.php
        header("Location: main.php");
        exit();
    }
    if(isset($_GET["id"])){
      // if this page was reached from an edit request, retrieve current values of this story from the database
      $story_id = htmlentities($_GET["id"]);
      require "database.php";
      $stmt = $mysqli->prepare("select title, story_text, ext_link from stories where pk_story_id = ".$story_id);
      $stmt->execute();
            if($stmt->errno){
          echo "Error while retrieving saved story details ".$stmt->error;
          exit;
      }
      $result = $stmt->get_result();
      $row = $result->fetch_assoc(); // this story's details are storied in $row associative array
      $stmt->close();
      // retrieve saved values
      $title = $row["title"];
      $body = $row["story_text"];
      $ext_link = $row["ext_link"];
    }else{
      // this is a submission of a new story; initialize values to null
      $title = "";
      $body = "";
      $ext_link = "";
      $imageData = null;
      $imageType = null;
    }

?>
<!doctype html>
<html lang="en">
  <head>
    <title>News⚡Flash</title>
    <link href="custom-stylesheet.css" rel="stylesheet" type="text/css" >
  </head>
  <body>
        <?php
        include "navbar.php";
        echo '<div class="newsfeed"><h1 class="text-center">Submit An Article</h1>';
        // Display errors which may have occured in file submission.
        // Alert code adapted from https://www.w3schools.com/bootstrap/bootstrap_alerts.asp -->
        if(isset( $_SESSION["article-status"])){
            echo "<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>".$_SESSION["article-status"];
            // once alert is displayed, wipe from session variable
            unset($_SESSION["article-status"]);
        }
        ?>
        <!-- Create form for new article content -->
        <form method="post" action="<?php
          if(isset($_GET["id"])){
            // submit with id in order to edit
            echo "process-article.php?id=".$story_id;
          }else{
            // submit w/o an id to create a new story
            echo "process-article.php";
          }
           ?>" enctype="multipart/form-data" >
            <label for="story-title">Title</label>
            <input type="text" id="story-title" maxlength="100" required name="story-title" value= "<?php echo  htmlentities($title);?>"><br>
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <label for="story-text">Story</label>
            <textarea id="story-text" rows="10" maxlength = "65535" name="story-text" ><?php echo $body;?></textarea><br>
            <label for="link">External News Link (optional)</label>
            <!-- Used max length for a url to set the constraint here. -->
            <input type="text" class="form-control" id="link" maxlength="2000" name="link" value = "<?php echo htmlentities($ext_link); ?>"><br>
          <?php
          //only allow image upload if this is a new submission, not an edit
            if(!isset($_GET["id"])){
              echo '<label for="image">Select an image to accompany your story (optional).</label>
                   <input type="file" id="image" name="story-image"><br>';
            }
          ?>
              <input type="submit"  value="Submit Story" name="submit-new-story"/>
              <a href="main.php">Cancel</a>
        </form>
        </div>
</body>
</html>
