<!doctype html>
<html lang="en">
  <head>
    <title>News⚡Flash</title>
    <link href="custom-stylesheet.css" rel="stylesheet" type="text/css" >
  </head>
  <body>
    <?php
      session_start();
      include "navbar.php";
      echo "<div class='newsfeed'>";
      // select info from database
        require 'database.php';
        $stmt = $mysqli->prepare("select * from stories order by title");
        if(!$stmt){
          printf("Query Prep Failed: %s\n", $mysqli->error);
          exit;
        }
        $stmt->execute();
        $result = $stmt->get_result();
        $count = 0; // use to break into rows of 3 stories
        // loop through all stories in the database
        while($row = $result->fetch_assoc()){
           if($row['story_image'] != null){
            // if an image was submitted with the story, display here
              echo "<img src='data:".$row['image_type'].";base64,".base64_encode( $row['story_image'])."' alt='story image' class = 'story-img'>";
           }else{
            // display default "news" image
              echo "<img src='default_img.jpg' alt='story image' class = 'story-img'>";
           }
           echo "<h3>".$row['title']."<small>  <a href='view-account.php?user=".$row["author"]."'>".$row["author"]."</a></small></h3>";
          // isolate the lead of this story text to display here
          if(strlen($row['story_text']) > 150){
            $displayCutPos=strpos($row["story_text"], ' ', 150);
            $storyLead = substr($row["story_text"],0,$displayCutPos)."...";
          } else{
            $storyLead = $row["story_text"];
          }
          $story_id = $row["pk_story_id"];
          echo "<p>".$storyLead."</p>
                <!-- provide read more link to article-view script and use URL method to pass unique story id to the article php script. -->
                <p><a href='article.php?id=".urlencode($story_id)."' class='btn btn-primary' role='button'>Read More</a>";
          if(isset($_SESSION["username"])) {
            if($_SESSION["username"] == $row["author"]){
              //if current user owns this story, display a delete and edit button
              echo " <a href='submit-article.php?id=".urlencode($story_id)."'>Edit</a>";
              echo " <a href='delete.php?id=".urlencode($story_id)."'>Delete</a>";
            }
          }
          echo "<hr>";
        } // /while loop
        $stmt->close();
      ?>
      </div>
</body>
</html>
