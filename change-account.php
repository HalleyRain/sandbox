<!DOCTYPE html>
<head>
    <title>News⚡Flash</title>
    <link href="custom-stylesheet.css" rel="stylesheet" type="text/css" >
</head>
<body>
	<?php
	// only allow this page to be visited if someone one is logged in
	session_start();
	if(!isset($_SESSION["username"])){
		header("Location: main.php");
	}else{
        $username = $_SESSION["username"];
    }
	include "navbar.php"; ?>
	<div class="newsfeed">
		<h3>Enter a new password to replace your old password.<br><small>You will be automatically logged out of your account.</small></h3><hr>
		<?php
		// Display errors which may have occured in account creation.
        // Alert code adapted from https://www.w3schools.com/bootstrap/bootstrap_alerts.asp -->
        if(isset( $_SESSION["account-status"])){
            echo $_SESSION["account-status"];
            // once alert is displayed, wipe from session variable
            unset($_SESSION["account-status"]);
        }
		?>
		<form method="POST" action="process-account.php">
				<label for="username">New Password</label><!--create textbox that accepts username -->
				<input type="password"  maxlength="255" required name="password" >
        <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
				<!--submit form to edit account & redirect to main page -->
				<input type="submit" value="Update Password" name="create-account"/>
		</form>

	</div><!-- /panel -->
</body>
</html>
