<?php
    session_start();
    // retrieve story and comment id's from URL
    $story_id=htmlentities($_GET["sid"]);
        // check that a user is logged in
    if(!isset($_SESSION["username"]) OR ($story_id == null)){
        header("Location: article.php?id=".$story_id);
        exit;
    }
    $isEdit = FALSE;
    if(isset($_GET["cid"])){
        $comment_id = htmlentities($_GET["cid"]);
        $isEdit = TRUE;
    }

    if (!$isEdit) {
        // process new comment and send to database
        $comment_text = htmlentities($_POST['comment']);
        // CSRF protection
        if(!hash_equals($_SESSION['token'], $_POST['token'])){
	         die("Request forgery detected");
        }
        require "database.php";
        //retrieve the author of the story
        $stmt = $mysqli->prepare("select author from stories where pk_story_id=?");
        if(!$stmt){
          printf("Query Prep Failed for retrieving author for story: %s\n", $mysqli->error);
          header("Location: article.php?id=".$story_id);
          exit;
        }
        $stmt->bind_param('s', $story_id);
        $stmt->execute();
        if($stmt->errno){
            printf("retrieving author for story: ".$stmt->error);
            header("Location: article.php?id=".$story_id);
            exit;
        }
        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
        $story_author = $row["author"];
        $stmt->close();

        $stmt = $mysqli->prepare("insert into comments (commenter, story_id, comment_text, story_author) values (?, ?, ?, ?)");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            header("Location: article.php?id=".$story_id);
            exit;
        }
        $stmt->bind_param('ssss', $_SESSION["username"], $story_id, $comment_text, $story_author );
        $stmt->execute();
        if($stmt->errno){
            printf("inserting author for story: ".$stmt->error);
            header("Location: article.php?id=".$story_id);
            exit;
        }
        $stmt->close();
        // store the id of the comment just inserted
        $comment_id = $mysqli->insert_id;
        // return to article page to view new comment
        header("Location: article.php?id=".$story_id."#".$comment_id."comment");
    }else{
        // Edit a comment instead of insert one
        $comment_text = htmlentities($_POST['comment-update']);
        // CSRF protection
        if(!hash_equals($_SESSION['token'], $_POST['token'])){
	         die("Request forgery detected");
        }
        require "database.php";
        $stmt = $mysqli->prepare("update comments set comment_text=? where pk_comment_id=?");
        if(!$stmt){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            header("Location: article.php?id=".$story_id);
            exit;
        }
        $stmt->bind_param('sd', $comment_text, $comment_id);
        $stmt->execute();
        $stmt->close();
        // return to article page to view updated comment
        header("Location: article.php?id=".$story_id."#".$comment_id."comment");
    }
?>
