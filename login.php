<?php
// php script to catch input from login form and validate against database
    // verify that a user is logged in before visiting this page; otherwise redirect to main page
    session_start();
    require 'database.php';
    // Use a prepared statement
    $stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM users WHERE username=?");
    // Bind the parameter
    $stmt->bind_param('s', $username);
    $username = $_POST['username'];
    $pwd_guess = $_POST['password'];
    $stmt->execute();
    if($stmt->errno){
        echo "login ".$stmt->error;
        exit();
    }
    // Bind the results
    $stmt->bind_result($cnt, $username, $real_password);
    $stmt->fetch();
    // Compare the submitted password to the actual password hash
    if($cnt == 1 && password_verify($pwd_guess, $real_password)){
    	// Login success, go to home page as user
    	$_SESSION['username'] = $username;
        $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(32)); // generate a 32-byte random string
      header("Location: main.php");
    } else{
    	// Login fail, go back to home page as guest
      header("Location: main.php");
    }
