<!doctype html>
<html lang="en">
  <head>
  <title>News⚡Flash</title>
    <link href="custom-stylesheet.css" rel="stylesheet" type="text/css" >
  </head>
  <body>
    <?php
    session_start();
    // grab passed id from URL
    $story_id = htmlentities(urldecode($_GET['id']));
    // check if the user is editing a comment
    $isEdit = isset($_GET["edit"]) AND isset($_SESSION["username"]);
    require "database.php";
    $stmt = $mysqli->prepare("select * from stories where pk_story_id = ".$story_id);
    if(!$stmt){
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc(); // this story's details are storied in $row associative array
    $stmt->close();

    include "navbar.php";
// display story title and author as page header
    echo "<div class = 'newsfeed'><h1>".$row["title"]."<small> By <a href='view-account.php?user=".$row["author"]."'> ".$row["author"]."</a></small></h1>";
    if($row['story_image'] != null){
        // if an image was submitted with the story, display here
          echo "<img src='data:".$row['image_type'].";base64,".base64_encode( $row['story_image'])."' alt='story image' class='read-more-img'>";
    }
    if($row['ext_link'] != null){
      // if a link was submitted with the story, display here as a link
    echo "<a href=".htmlentities($link).">External Link to Story</a>";
    }
    //nl2br function preserves line breaks by converting /n to <br>
    echo "<p class = 'read-more-txt'>".nl2br($row["story_text"])."</p>";
            ?>
        <div class="panel-footer">
          <h3>Comments</h3>
          <ul>
          <?php
            $stmt = $mysqli->prepare("select * from comments where story_id = ".$story_id);
            if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->execute();
            $result = $stmt->get_result();
            while($row = $result->fetch_assoc()){
                // Story's comments stored in each row. Iterate through to display comments.
                $comment = $row["comment_text"];
                $comment_author = $row["commenter"];
                $comment_id = $row["pk_comment_id"];
                echo '<li><h4>'.$comment_author.'</h4>'.$comment;
                if(isset($_SESSION["username"])){
                  if ($comment_author==$_SESSION["username"]){
                    // if this comment belongs to the logged in user, display an edit and delete option
                    // create a div with id so after edit, page reloads at this location
                    echo '<div id="'.$row["pk_comment_id"].'comment">
                      <a href = "article.php?id='.$story_id.'&edit='.$comment_id.'#'.$comment_id.'comment">Edit</a>
                      <a href = "delete.php?cid='.urlencode($comment_id).'&sid='.urlencode($story_id).'">Delete</a>
                    </div>';
                  }
                }
                if ($isEdit){
                  if (($comment_author==$_SESSION["username"]) AND ($comment_id == htmlentities($_GET["edit"]))){
                    // if the logged in user owns this comment and edit has been selected for it, display edit box here
                    echo '<form method="post" action = "process-comment.php?sid='.$story_id.'&cid='.$comment_id.'">
                          <input type="hidden" name="token" value="'.$_SESSION['token'].'">
                          <input type="text" maxlength = "255" required name ="comment-update" value = '.$comment.'>
                          <input type="submit" class = "btn btn-default" name ="Update" value="Update">
                          </form>';
                  }
                }
                echo '</li>';
            } // end while loop
            $stmt->close();

            echo "</ul><hr>";

            //Beneath displayed comments, provide form to accept new comments if a user is logged in.
            // input bar format adapted from : https://getbootstrap.com/docs/3.3/components/#input-groups
            if(isset($_SESSION["username"])){
                echo '<form method="post" action = "process-comment.php?sid='.$story_id.'">
                        <input type="text"  maxlength="255" name ="comment" required placeholder="Leave a comment as '.$_SESSION["username"].'">
                        <input type="hidden" name="token" value="'.$_SESSION['token'].'" />
                        <input type="submit" name ="Post" value="Post">
                      </form>';
            }
        ?>
      </div><!--/panel footer-->
    </div><!-- /panel -->
  </body>
</html>
