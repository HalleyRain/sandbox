      <div id="navbar">
        <h3><a href="main.php">News⚡Flash</a></h3>
      <!-- text only displays when user is signed in -->
        <?php
            if(isset($_SESSION["username"])){
                echo "Signed in as ".$_SESSION["username"]."<br>";
        } 
                if(!isset($_SESSION["username"])){
                    // if no one is logged in, display login menu
                    echo '<form method="post" action="login.php">
                            <input type="text" placeholder="Username" name="username" required>
                            <input type="password" placeholder="Password" name="password" required>
                            <input type="submit" value = "Sign In" >
                        </form>
                        <a href="new-account.php">Create New Account</a>';
            }else{
                // someone is logged in, display account options menu
                echo '<a href="submit-article.php">Submit Article</a><br>
                <a href="view-account.php?user='.$_SESSION["username"].'">View Account</a><br>
                    <a href="logout.php">Logout</a>';
            }
            ?>
      </div>
